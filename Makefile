# --------------------------------------------------------------
#!/usr/bin/make -f
# Makefile for DISTRHO Plugins #
# ---------------------------- #
# Created by falkTX
#

# --------------------------------------------------------------
# Project name, used for binaries

NAME = transientMangler

# --------------------------------------------------------------
# Files to build

FILES_DSP  = \
			plugin/plugin.cpp \
			dsp/buffer.cpp \
			dsp/onepole.cpp \
			dsp/ADSR.cpp \
			aubio_module/src/hopbuffer.cpp \
			aubio_module/src/aubio_module.cpp \
			aubio_module/src/aubio_onset.cpp

FILES_UI  = \
			UI/UI.cpp \
			UI/artwork.cpp \
			UI/fonts/NotoSans_Regular.ttf.cpp \

# --------------------------------------------------------------
# Do some magic

include ../../dpf/Makefile.plugins.mk


BUILD_CXX_FLAGS += \
				   -I./plugin \
				   -I./dsp \
				   -I./UI \
				   -I./UI/fonts \
				   -I./aubio_module/src \

ifeq ($(WIN32),true)
LINK_OPTS += -static -static-libgcc -static-libstdc++
endif

BUILD_CXX_FLAGS += -laubio
# --------------------------------------------------------------
# Enable all possible plugin types

ifeq ($(HAVE_DGL),true)
ifeq ($(HAVE_JACK),true)
TARGETS += jack
endif
endif

ifeq ($(HAVE_DGL),true)
TARGETS += lv2_sep
else
TARGETS += lv2_dsp
endif

TARGETS += vst

all: $(TARGETS)

# --------------------------------------------------------------
