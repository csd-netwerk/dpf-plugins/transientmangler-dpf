#include "plugin.hpp"
#include <iostream>

START_NAMESPACE_DISTRHO


// -----------------------------------------------------------------------

TransientMangler::TransientMangler()
    : Plugin(paramCount, 1, 0) // 1 program, 0 states
{
    loadProgram(0);

    sampleRate = (float)getSampleRate();
    bufferSize = getBufferSize();
    // set default values

    buffers = new TransientBuffer*[2];

    lowpass.setFc(100.0/(double)sampleRate);

    onset_detector.setOnsetThreshold(0.7f);
    onset_detector.setSilenceThreshold(-90.0f);
    onset_detector.setBuffersize(bufferSize);
    onset_detector.setSamplerate(sampleRate);
    onset_detector.setOnsetMethod("complex");


    std::cout << "sampleRate from constructor = " << sampleRate << std::endl;


    for (unsigned i = 0; i < 2; i++)
        buffers[i] = new TransientBuffer();


    threshold = 0.0001;
    triggerControl = 0.0;
    pitch = 1.0;
    dryWet = 1.0;
    glitch = 0.0;
    attack =0.01;
    decay  = 0.2;
    sustain = 0.8;
    release = 0.2;

    // reset
    reset();
}

TransientMangler::~TransientMangler()
{
}

// -----------------------------------------------------------------------
// Init

void TransientMangler::initParameter(uint32_t index, Parameter& parameter)
{
    switch (index)
    {
    case paramThreshold:
        parameter.hints      = kParameterIsAutomable;
        parameter.name       = "Threshold";
        parameter.symbol     = "Threshold";
        parameter.unit       = "";
        parameter.ranges.def = 0.1f;
        parameter.ranges.min = 0.1f;
        parameter.ranges.max = 1.f;
        break;
    case paramTrigger:
        parameter.hints      = kParameterIsAutomable | kParameterIsTrigger;
        parameter.name       = "Trigger";
        parameter.symbol     = "Trigger";
        parameter.unit       = "";
        parameter.ranges.def = 0.f;
        parameter.ranges.min = 0.0f;
        parameter.ranges.max = 1.f;
        break;
    case paramPitch:
        parameter.hints      = kParameterIsAutomable;
        parameter.name       = "Pitch";
        parameter.symbol     = "Pitch";
        parameter.unit       = "";
        parameter.ranges.def = 1.f;
        parameter.ranges.min = 0.1f;
        parameter.ranges.max = 1.f;
        break;
    case paramDryWet:
        parameter.hints      = kParameterIsAutomable;
        parameter.name       = "DryWet";
        parameter.symbol     = "DryWet";
        parameter.unit       = "";
        parameter.ranges.def = 1.f;
        parameter.ranges.min = 0.0f;
        parameter.ranges.max = 1.f;
        break;
    case paramGlitch:
        parameter.hints      = kParameterIsAutomable;
        parameter.name       = "Glitch";
        parameter.symbol     = "Glitch";
        parameter.unit       = "";
        parameter.ranges.def = 1.f;
        parameter.ranges.min = 0.0f;
        parameter.ranges.max = 1.f;
        break;
    case paramAttack:
        parameter.hints      = kParameterIsAutomable;
        parameter.name       = "A";
        parameter.symbol     = "A";
        parameter.unit       = "";
        parameter.ranges.def = 0.f;
        parameter.ranges.min = 0.0f;
        parameter.ranges.max = 1.f;
        break;
    case paramDecay:
        parameter.hints      = kParameterIsAutomable;
        parameter.name       = "D";
        parameter.symbol     = "D";
        parameter.unit       = "";
        parameter.ranges.def = 1.f;
        parameter.ranges.min = 0.1f;
        parameter.ranges.max = 1.f;
        break;
    case paramSustain:
        parameter.hints      = kParameterIsAutomable;
        parameter.name       = "S";
        parameter.symbol     = "S";
        parameter.unit       = "";
        parameter.ranges.def = 1.f;
        parameter.ranges.min = 0.0f;
        parameter.ranges.max = 1.f;
        break;
    case paramRelease:
        parameter.hints      = kParameterIsAutomable;
        parameter.name       = "R";
        parameter.symbol     = "R";
        parameter.unit       = "";
        parameter.ranges.def = 1.f;
        parameter.ranges.min = 0.0f;
        parameter.ranges.max = 1.f;
        break;
    }
}

void TransientMangler::initProgramName(uint32_t index, String& programName)
{
    if (index != 0)
        return;

    programName = "Default";
}

// -----------------------------------------------------------------------
// Internal data

float TransientMangler::getParameterValue(uint32_t index) const
{
    switch (index)
    {
    case paramThreshold:
        return threshold;
    case paramTrigger:
        return triggerControl;
    case paramPitch:
        return pitch;
    case paramDryWet:
        return dryWet;
    case paramGlitch:
        return glitch;
    case paramAttack:
        return attack;
    case paramDecay:
        return decay;
    case paramSustain:
        return sustain;
    case paramRelease:
        return release;
    }
}

void TransientMangler::setParameterValue(uint32_t index, float value)
{
    switch (index)
    {
    case paramThreshold:
        threshold = value;
        break;
    case paramTrigger:
        triggerControl = value;
        break;
    case paramPitch:
        pitch = value;
        break;
    case paramDryWet:
        dryWet = value;
        break;
    case paramGlitch:
        glitch = value;
        break;
    case paramAttack:
        attack = value;
        break;
    case paramDecay:
        decay = value;
        break;
    case paramSustain:
        sustain = value;
        break;
    case paramRelease:
        release = value;
        break;
    }
}

void TransientMangler::loadProgram(uint32_t index)
{
}

void TransientMangler::reset()
{
}

// -----------------------------------------------------------------------
// Process

void TransientMangler::activate()
{
}

void TransientMangler::deactivate()
{
}



void TransientMangler::run(const float** inputs, float** outputs, uint32_t frames)
{
    const float* input  = inputs[0];
    float*       output = outputs[0];

    // Aubio Onset detection processing
    bool is_onset = onset_detector.process(const_cast<float*>(input));

    // ADSR setup
    adsr.setAttackRate(attack * sampleRate);  // .1 second
    adsr.setDecayRate(decay * sampleRate);
    adsr.setReleaseRate(release * sampleRate);
    adsr.setSustainLevel(sustain);

    // Main processing body
    for (uint32_t f = 0; f < frames; ++f)
    {
        // Clearing output buffer from random garbage (if not in-place processing)
        if (input != output) output[f] = 0.f;

        double sample = input[f];

        double signal = 0.0;
        for (unsigned b = 0; b < 2; b++) {
            buffers[b]->envelope();
            buffers[b]->setPitch(pitch);
            buffers[b]->fillBuffer(sample);
            signal += buffers[b]->getSample();
            buffers[b]->playBuffer();
        }

        if (is_onset) {
            adsr.gate(true);
            buffers[bIndex]->setAttackRelease(2);
            bIndex ^= 1;
            buffers[bIndex]->resetBuffer();
            buffers[bIndex]->setAttackRelease(1);
            is_onset = false;
        }

        output[f] = ((signal * dryWet) * lowpass.process(adsr.process())) + (sample * ((dryWet * -1) + 1));

    }
}



// -----------------------------------------------------------------------

Plugin* createPlugin()
{
    return new TransientMangler();
}

// -----------------------------------------------------------------------

END_NAMESPACE_DISTRHO
