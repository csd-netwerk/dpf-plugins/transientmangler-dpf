#include "buffer.hpp"
#include <iostream>


TransientBuffer::TransientBuffer()
{
    for (unsigned i = 0; i < BUFFER_SIZE; i++) {
        buffer[i] = 0.0;
    }

}


TransientBuffer::~TransientBuffer()
{
}

void TransientBuffer::setAttackRelease(int state)
{
    envState = state;
}

float TransientBuffer::envelope()
{
    switch (envState)
    {
        case 0:
            gain = 0.0;
            break;
        case 1:
            if (gain < 1.0) {
                gain += 0.01;
            } else {
                gain = 1.0;
            }
            break;
        case 2:
            if (gain > 0.0) {
                gain -= 0.01;
            } else {
                envState = 0;
            }
            break;
    }
    return gain;
}

void TransientBuffer::fillBuffer(float sample)
{
    if (recordingIndex < BUFFER_SIZE) {
        recordingIndex++;
    } else {
        recordingIndex = 0;
    }
    buffer[recordingIndex] = sample;
}


void TransientBuffer::playBuffer()
{
    sample = interpolate_sample();
    playIndex += playBackSpeed;
    playIndex = (playIndex < (float)BUFFER_SIZE) ? playIndex : 0.0;
}


void TransientBuffer::resetBuffer()
{
    playIndex = 0.0;
    recordingIndex = 0.0;
}


void TransientBuffer::setPitch(float playBackSpeed)
{
    this->playBackSpeed = playBackSpeed;
}


void TransientBuffer::setSampleLength(int sampleLength)
{
    this->sampleLength = sampleLength;
}


double TransientBuffer::getSample()
{
    return sample * gain;
}


double TransientBuffer::interpolate_sample()
{
    int x0 = (int)playIndex;
    int x1 = (int)(playIndex + 1.0);
    int x2 = (int)(playIndex + 2.0);
    int xm1 = (int)(playIndex - 1.0);
    double x = playIndex - x0;
    double f0, f1, f2, fm1;
    if (x1 >= BUFFER_SIZE) x1 -= BUFFER_SIZE;
    if (x2 >= BUFFER_SIZE) x2 -= BUFFER_SIZE;
    if (xm1 < 0) xm1 += BUFFER_SIZE;
    f0 = buffer[x0];
    f1 = buffer[x1];
    f2 = buffer[x2];
    fm1 = buffer[xm1];

    return f0 + (((f2 - fm1 - 3*f1 + 3*f0) * x + 3 * (f1 + fm1 - 2*f0)) * x - 
            (f2 + 2*fm1 - 6*f1 + 3*f0)) * x / 6.0;
}
