#ifndef DISTRHO_UI_NEKOBI_HPP_INCLUDED
#define DISTRHO_UI_NEKOBI_HPP_INCLUDED

#include "DistrhoUI.hpp"

#include "NanoVG.hpp"
#include "NotoSans_Regular.ttf.hpp"
#include "ImageWidgets.hpp"
#include <vector>

#include "artwork.hpp"

using DGL_NAMESPACE::ImageAboutWindow;
using DGL_NAMESPACE::ImageButton;
using DGL_NAMESPACE::ImageKnob;
using DGL_NAMESPACE::ImageSlider;
using DGL_NAMESPACE::ImageSwitch;

#define NUM_TOGGLES 5

START_NAMESPACE_DISTRHO

// -----------------------------------------------------------------------

class TransientManglerUI : public UI,
                        public ImageButton::Callback,
                        public ImageKnob::Callback,
                        public ImageSlider::Callback,
                        public ImageSwitch::Callback
{
public:
    TransientManglerUI();
    ~TransientManglerUI();

protected:
    // -------------------------------------------------------------------
    // DSP Callbacks

    void parameterChanged(uint32_t index, float value) override;

    // -------------------------------------------------------------------
    // UI Callbacks

    void uiIdle() override;

    // -------------------------------------------------------------------
    // Widget Callbacks
    bool onMouse(const MouseEvent& ev) override;
    void imageButtonClicked(ImageButton* button, int) override;
    void imageKnobDragStarted(ImageKnob* knob) override;
    void imageKnobDragFinished(ImageKnob* knob) override;
    void imageKnobValueChanged(ImageKnob* knob, float value) override;
    void imageSliderDragStarted(ImageSlider* slider) override;
    void imageSliderDragFinished(ImageSlider* slider) override;
    void imageSliderValueChanged(ImageSlider* slider, float value) override;
    void imageSwitchClicked(ImageSwitch* imageSwitch, bool down) override;
    void changeEncoderValue(ImageKnob* slider, float value);
    void onDisplay() override;
    void programLoaded(uint32_t index) override;

private:
    Image fImgBackground;

    ScopedPointer<ImageKnob>   transientThreshold;
    ScopedPointer<ImageKnob>   playbackSpeed;
    ScopedPointer<ImageKnob>   mix;
    ScopedPointer<ImageSlider> envAttack;
    ScopedPointer<ImageSlider> envDecay;
    ScopedPointer<ImageSlider> envSustain;
    ScopedPointer<ImageSlider> envRelease;

    ScopedPointer<ImageSwitch> trigger;

    DISTRHO_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(TransientManglerUI)
};

// -----------------------------------------------------------------------

END_NAMESPACE_DISTRHO

#endif // DISTRHO_UI_NEKOBI_HPP_INCLUDED
