#ifndef DISTRHO_PLUGIN_SLPLUGIN_HPP_INCLUDED
#define DISTRHO_PLUGIN_SLPLUGIN_HPP_INCLUDED

#include "DistrhoPlugin.hpp"
#include "buffer.hpp"
#include "ADSR.h"
#include "onepole.hpp"
#include "aubio_onset.hpp"


START_NAMESPACE_DISTRHO

class TransientMangler : public Plugin
{
public:
    enum Parameters
    {
        paramThreshold = 0,
        paramTrigger,
        paramPitch,
        paramDryWet,
        paramGlitch,
        paramAttack,
        paramDecay,
        paramSustain,
        paramRelease,
        paramCount
    };

    TransientMangler();
    ~TransientMangler();

protected:
    // -------------------------------------------------------------------
    // Information

    const char* getLabel() const noexcept override
    {
        return "TransientMangler";
    }

    const char* getDescription() const override
    {
        return "Transient Mangler";
    }

    const char* getMaker() const noexcept override
    {
        return "CSD";
    }

    const char* getHomePage() const override
    {
        return "http://";
    }

    const char* getLicense() const noexcept override
    {
        return "Custom";
    }

    uint32_t getVersion() const noexcept override
    {
        return d_version(1, 0, 8);
    }

    int64_t getUniqueId() const noexcept override
    {
        return d_cconst('C', 'S', 'D', 's');
    }

    // -------------------------------------------------------------------
    // Init

    void initParameter(uint32_t index, Parameter& parameter) override;
    void initProgramName(uint32_t index, String& programName) override;

    // -------------------------------------------------------------------
    // Internal data

    float getParameterValue(uint32_t index) const override;
    void  setParameterValue(uint32_t index, float value) override;
    void  loadProgram(uint32_t index) override;

    // -------------------------------------------------------------------
    // Process
    void activate() override;
    void deactivate() override;
    void run(const float** inputs, float** outputs, uint32_t frames) override;

private:

    TransientBuffer **buffers;
    OnePole lowpass;
    AubioOnset onset_detector;
    ADSR adsr; //TODO need to change this class name when we port the plugin to JUCE

    int bIndex = 0;
    double pSignal = 0.0;
    float threshold;
    float triggerControl;
    float pitch;
    float dryWet;
    float glitch;
    float attack;
    float decay;
    float sustain;
    float release;

    float sampleRate;
    uint32_t bufferSize;

    bool trigger = false;

    void reset();

    DISTRHO_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(TransientMangler)
};

// -----------------------------------------------------------------------

END_NAMESPACE_DISTRHO

#endif  // DISTRHO_PLUGIN_SLPLUGIN_HPP_INCLUDED



