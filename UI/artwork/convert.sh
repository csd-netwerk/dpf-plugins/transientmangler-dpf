#!/bin/bash

INPUT_FILE="$1"

read -p "Enter Output File Name: "  OUTPUT_FILE
read -p "Enter Image Size: "  SIZE

convert "$INPUT_FILE" -define h:format=bgr -depth 8 -size "$SIZE" "headers/$OUTPUT_FILE"
