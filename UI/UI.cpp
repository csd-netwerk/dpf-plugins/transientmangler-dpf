#include "plugin.hpp"
#include "UI.hpp"
#include <iostream>
#include <array>
#include <vector>
#include <math.h>
#include <string>

START_NAMESPACE_DISTRHO

namespace Art = TransientManglerArtwork;

// -----------------------------------------------------------------------

TransientManglerUI::TransientManglerUI()
    : UI(Art::backgroundWidth, Art::backgroundHeight),
      fImgBackground(Art::backgroundData, Art::backgroundWidth, Art::backgroundHeight, GL_BGR)
{

    transientThreshold = new ImageKnob(this,
            Image(Art::knobData, Art::knobWidth, Art::knobHeight, GL_BGRA), ImageKnob::Vertical);
    transientThreshold->setId(TransientMangler::paramThreshold);
    transientThreshold->setAbsolutePos(150, 200);
    transientThreshold->setRange(0.0f, 1.0f);
    transientThreshold->setDefault(0.0f);
    transientThreshold->setImageLayerCount(100);
    transientThreshold->setCallback(this);


    playbackSpeed = new ImageKnob(this,
            Image(Art::knobData, Art::knobWidth, Art::knobHeight, GL_BGRA), ImageKnob::Vertical);
    playbackSpeed->setId(TransientMangler::paramPitch);
    playbackSpeed->setAbsolutePos(500, 160);
    playbackSpeed->setRange(0.0f, 1.0f);
    playbackSpeed->setDefault(0.0f);
    playbackSpeed->setImageLayerCount(100);
    playbackSpeed->setCallback(this);


    mix = new ImageKnob(this,
            Image(Art::knobData, Art::knobWidth, Art::knobHeight, GL_BGRA), ImageKnob::Vertical);
    mix->setId(TransientMangler::paramDryWet);
    mix->setAbsolutePos(500, 340);
    mix->setRange(0.0f, 1.0f);
    mix->setDefault(0.0f);
    mix->setImageLayerCount(100);
    mix->setCallback(this);

    // slider
    Image sliderImage(Art::sliderData, Art::sliderWidth, Art::sliderHeight);


    int envelopeX = 735;
    int envelopeXOffset = 114;
    int envelopeStartY = 219;
    int envelopeEndY = 352;
    //int envelopeEndY = 219;
    //int envelopeStartY = 352;

    envAttack = new ImageSlider(this, sliderImage);
    envAttack->setId(TransientMangler::paramAttack);
    envAttack->setStartPos(envelopeX, envelopeStartY);
    envAttack->setEndPos(envelopeX, envelopeEndY);
    envAttack->setInverted(true);
    envAttack->setRange(0.0f, 1.0f);
    envAttack->setValue(0.0f);
    envAttack->setCallback(this);

    envDecay = new ImageSlider(this, sliderImage);
    envDecay->setId(TransientMangler::paramDecay);
    envDecay->setStartPos(envelopeX + (envelopeXOffset * 1), envelopeStartY);
    envDecay->setEndPos(envelopeX, envelopeEndY);
    envDecay->setInverted(true);
    envDecay->setRange(0.0f, 1.0f);
    envDecay->setValue(0.0f);
    envDecay->setCallback(this);

    envSustain = new ImageSlider(this, sliderImage);
    envSustain->setId(TransientMangler::paramSustain);
    envSustain->setStartPos(envelopeX + (envelopeXOffset * 2), envelopeStartY);
    envSustain->setEndPos(envelopeX, envelopeEndY);
    envSustain->setInverted(true);
    envSustain->setRange(0.0f, 1.0f);
    envSustain->setValue(0.0f);
    envSustain->setCallback(this);

    envRelease = new ImageSlider(this, sliderImage);
    envRelease->setId(TransientMangler::paramRelease);
    envRelease->setStartPos(envelopeX + (envelopeXOffset * 3), envelopeStartY);
    envRelease->setEndPos(envelopeX, envelopeEndY);
    envRelease->setInverted(true);
    envRelease->setRange(0.0f, 1.0f);
    //envRelease->setStep(1.0f);
    envRelease->setValue(0.0f);
    envRelease->setCallback(this);


    trigger = new ImageSwitch(this,
            Image(Art::triggerOnData, Art::triggerOnWidth, Art::triggerOnHeight, GL_BGR),
            Image(Art::triggerOnData, Art::triggerOnWidth, Art::triggerOnHeight, GL_BGR));
    trigger->setId(TransientMangler::paramTrigger);
    trigger->setAbsolutePos(160, 385);
    trigger->setCallback(this);
}


TransientManglerUI::~TransientManglerUI()
{
}
// -----------------------------------------------------------------------
// DSP Callbacks

void TransientManglerUI::parameterChanged(uint32_t index, float value)
{
    switch (index)
    {
        case TransientMangler::paramThreshold:
            transientThreshold->setValue(value);
            break;
        case TransientMangler::paramPitch:
            playbackSpeed->setValue(value);
            break;
        case TransientMangler::paramDryWet:
            mix->setValue(value);
            break;
        case TransientMangler::paramAttack:
            envAttack->setValue(value);
            break;
        case TransientMangler::paramDecay:
            envDecay->setValue(value);
            break;
        case TransientMangler::paramSustain:
            envSustain->setValue(value);
            break;
        case TransientMangler::paramRelease:
            envRelease->setValue(value);
            break;
        case TransientMangler::paramTrigger:
            //trigger->isDown((bool)value);
            break;
    }
}

// -----------------------------------------------------------------------
// UI Callbacks

void TransientManglerUI::uiIdle()
{
}

// -----------------------------------------------------------------------
// Widget Callbacks

void TransientManglerUI::imageButtonClicked(ImageButton* button, int)
{
    //if (button != fButtonAbout)
    //    return;

}

void TransientManglerUI::imageKnobDragStarted(ImageKnob* knob)
{
    editParameter(knob->getId(), true);
}

void TransientManglerUI::imageKnobDragFinished(ImageKnob* knob)
{
    editParameter(knob->getId(), false);
}

void TransientManglerUI::imageKnobValueChanged(ImageKnob* knob, float value)
{
    setParameterValue(knob->getId(), value);
}

void TransientManglerUI::imageSliderDragStarted(ImageSlider* slider)
{
    editParameter(slider->getId(), true);
}

void TransientManglerUI::imageSliderDragFinished(ImageSlider* slider)
{
    editParameter(slider->getId(), false);
}

void TransientManglerUI::imageSliderValueChanged(ImageSlider* slider, float value)
{
    setParameterValue(slider->getId(), value);
}

void TransientManglerUI::imageSwitchClicked(ImageSwitch* imageSwitch, bool down)
{
    const uint buttonId(imageSwitch->getId());

    editParameter(buttonId, true);
    setParameterValue(buttonId, down ? 1.0f : 0.0f);
    editParameter(buttonId, false);

    repaint();
}

void TransientManglerUI::changeEncoderValue(ImageKnob* knob, float value)
{
    editParameter(knob->getId(), true);
    setParameterValue(knob->getId(), value);
    editParameter(knob->getId(), false);
}


void TransientManglerUI::programLoaded(uint32_t index)
{
}


void TransientManglerUI::onDisplay()
{
    fImgBackground.draw();

}

bool TransientManglerUI::onMouse(const MouseEvent& ev)
{
    return true;
}
// -----------------------------------------------------------------------

UI* createUI()
{
    return new TransientManglerUI();
}

// -----------------------------------------------------------------------

END_NAMESPACE_DISTRHO
