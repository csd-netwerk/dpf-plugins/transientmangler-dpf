/* (Auto-generated binary data file). */

#ifndef BINARY_DISTRHOARTWORK_TRANSIENTMANGLER_HPP
#define BINARY_DISTRHOARTWORK_TRANSIENTMANGLER_HPP

namespace TransientManglerArtwork
{
    extern const char* backgroundData;
    const unsigned int backgroundDataSize = 2071395;
    const unsigned int backgroundWidth    = 1205;
    const unsigned int backgroundHeight   = 573;

    extern const char* knobData;
    const unsigned int knobDataSize = 2560000;
    const unsigned int knobWidth    = 80;
    const unsigned int knobHeight   = 8000;

    extern const char* sliderData;
    const unsigned int sliderDataSize = 8184;
    const unsigned int sliderWidth    = 66;
    const unsigned int sliderHeight   = 31;

    extern const char* triggerOnData;
    const unsigned int triggerOnDataSize = 11907;
    const unsigned int triggerOnWidth    = 63;
    const unsigned int triggerOnHeight   = 63;

    extern const char* triggerOffData;
    const unsigned int triggerOffDataSize = 11907;
    const unsigned int triggerOffWidth    = 63;
    const unsigned int triggerOffHeight   = 63;

}

#endif // BINARY_DISTRHOARTWORKPEDALMANGLER_HPP
