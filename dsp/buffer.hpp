#ifndef _H_TRANSIENT_BUFFER
#define _H_TRANSIENT_BUFFER

#define BUFFER_SIZE 480000

class TransientBuffer
{
public:
    TransientBuffer();
    ~TransientBuffer();
    void setAttackRelease(int state);
    void fillBuffer(float sample);
    void playBuffer();
    void resetBuffer();
    void setPitch(float playBackSpeed);
    void setSampleLength(int sampleLength);
    float envelope();
    double getSample();
    double interpolate_sample();
private:
    int envState = 1;
    int sampleLength = 0;
    int recordingIndex = 0;
    float gain = 1.0;
    float playIndex = 0;
    float sample = 0.0;
    float playBackSpeed = 1.0;
    float buffer[BUFFER_SIZE];
};

#endif //_H_TRANSIENT_BUFFER

